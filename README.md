# tomotools training

This repository contains training material on the tomography tools used at ESRF. 

It focuses on two interfaces:  
* Command Line Interface (CLI)
* Python inerface. For this one it is recommended that you already have some python background.


Today tomotools software are organized as follow:

* nxtomomill: data conversion, some data reduction
* nabu: tomography processing library
* tomwer: workflow definition

Here is a drawing of it:

![tomotools software organization](https://gitlab.esrf.fr/tomotools/training/-/raw/main/img/software_organization.png)


The tomotools suite is firstly targetting ![HDF5](https://www.hdfgroup.org/solutions/hdf5/) files through ![NXtomo](https://manual.nexusformat.org/classes/applications/NXtomo.html) format.
But it also ensures backward compatibility with EDF files.

The recommended order of reading is:

1. nxtomomill
2. nabu
3. tomwer


## nxtomomill

* trainings: https://gitlab.esrf.fr/tomotools/training/-/tree/main/nxtomomill
* documentation: https://tomotools.gitlab-pages.esrf.fr/nxtomomill
* tutorials: http://www.silx.org/pub/doc/nxtomomill/latest/tutorials/index.html


## nabu

* trainings: https://gitlab.esrf.fr/tomotools/training/-/tree/main/nabu
* documentation: https://www.silx.org/pub/nabu/doc/index.html
* getting started: https://www.silx.org/pub/nabu/doc/index.html#getting-started


## tomwer
for now there is no training about tomwer.
But you can find:

* trainings: https://gitlab.esrf.fr/tomotools/training/-/tree/main/tomwer
* video tutorials: https://www.youtube.com/@tomotools
* documentation: https://tomotools.gitlab-pages.esrf.fr/tomwer/


# latest html version

The latest html version of the training can be found here: https://tomotools.gitlab-pages.esrf.fr/training/


# License

This work is licensed under the [Creative Commons Attribution 4.0 International Public License](https://creativecommons.org/licenses/by/4.0/>).

![CCBY](http://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg)
