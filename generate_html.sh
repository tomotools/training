mkdir -p build/html
mkdir -p build/html/nxtomo
mkdir -p build/html/nxtomomill
mkdir -p build/html/nabu
mkdir -p build/html/tomwer

cp .generate_html/index.html build/html/
cp -r img build/html/
jupyter-nbconvert --to html --embed-images *.ipynb --output-dir build/html
jupyter-nbconvert --to html --embed-images nxtomo/*.ipynb --output-dir build/html/nxtomo
jupyter-nbconvert --to html --embed-images nxtomomill/*.ipynb --output-dir build/html/nxtomomill
jupyter-nbconvert --to html --embed-images nabu/*.ipynb --output-dir build/html/nabu
jupyter-nbconvert --execute --to html --embed-images tomwer/*.ipynb --output-dir build/html/tomwer
