from nxtomo.application.nxtomo import NXtomo

scan = in_data

print("initial scan energy is", scan.energy)

# step 1: modify the energy using `nxtomo`
nx_tomo = NXtomo().load(
    scan.master_file,
    scan.entry
)
nx_tomo.energy = 12.3  # 19.0
nx_tomo.energy.units = "kev"

# step 2: overwrite the .nx file (this will not affect original bliss file !!!)
nx_tomo = nx_tomo.save(
    scan.master_file,
    scan.entry,
    overwrite=True,
)

# step 3: clear cache to make sure the mofications will be applied
scan.clear_caches()

print("new scan energy is", scan.energy)
out_data = scan
