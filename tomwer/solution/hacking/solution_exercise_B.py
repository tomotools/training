from tomwer.core.volume.volumefactory import VolumeFactory

scan = in_data

# step 1: retrieve reconstructions urls
reconstruction_identifiers = scan.latest_reconstructions
print(f"found reconstructions are", [str(recons_id) for recons_id in reconstruction_identifiers])

for i_slice, recons_id in enumerate(reconstruction_identifiers):
    # step 2: for each url retrieve the volume object
    volume = VolumeFactory.create_tomo_object_from_identifier(recons_id)
    # step 3: load reconstruction volume as a numpy array
    data = volume.load_data()
    # strep 4: print the shape of the slice
    print(f"slice {i_slice} shape is {data.shape}")
