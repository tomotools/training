{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "93e6deff",
   "metadata": {},
   "source": [
    "# Part 4: removing rings artefacts\n",
    "\n",
    "Rings artefacts can have many causes. They are detrimental for steps following reconstruction (e.g. segmentation). \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82e6ac3e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "In Nabu, two types of rings correction methods are available:\n",
    "  - Projections-based: \"double flat-field\"\n",
    "  - Sinogram-based: \"sinogram deringer\"\n",
    "  \n",
    "Volume-based rings corrections is not available yet."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a4b5563",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Preparing the dataset\n",
    "\n",
    "We will use another dataset (but still bamboo!) where plain reconstruction yields rings artefacts.  \n",
    "`/data/projects/tomo-sample-data/training/part4_rings/bamboo_rings.nx`\n",
    "\n",
    "\n",
    "Create a new directory and a new nabu configuration file:\n",
    "\n",
    "```bash\n",
    "mkdir my_dir\n",
    "cd my_dir\n",
    "nabu-config --dataset /data/projects/tomo-sample-data/training/part4_rings/bamboo_rings.nx\n",
    "```\n",
    "\n",
    "Perform a first reconstruction: `nabu nabu.conf --slice middle` and visualize it.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf36a342",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![bamboo reconstruction with rings](img/bamboo_rings.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ffd2c55",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1 - Projections-based rings removal (\"double flat-field\")\n",
    "\n",
    "\n",
    "This method consists in two steps:\n",
    "  - Compute a \"mean projection\" image `dff`\n",
    "  - Divide each projection with this mean projection\n",
    "\n",
    "![test](img/dff.png)\n",
    "\n",
    "In python:\n",
    "```python\n",
    "dff = projections.mean(axis=0) # sum the 3D stack over the angles\n",
    "projections /= dff # divide each projection with this image\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3919cfa",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Hopefully, when computing the \"mean projection\" `dff`, the sample-dependent features cancel out, and only the systematic noise remains. Thus, dividing the projections with `dff` will remove the systematic noise.\n",
    "\n",
    "Needless to say, quantitativeness is lost when using this method!\n",
    "\n",
    "\n",
    "In the configuration file, the parameter to use is `double_flatfield_enabled` in section `[preproc]`:\n",
    "\n",
    "```ini\n",
    "# ...\n",
    "[preproc]\n",
    "double_flatfield_enabled = 1\n",
    "# ...\n",
    "```\n",
    "\n",
    "(this option corresponds to `DOUBLEFFCORRECTION` in PyHST2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28d230dd",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "  - Modify the nabu configuration file to enable double flatfield\n",
    "  - Run the reconstruction of one slice and visualize it.\n",
    "  - Compare both reconstructions.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27b0855a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![bamboo reconstruction, dff enabled](img/bamboo_dff.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26ef72e2",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Advanced parameters\n",
    "\n",
    "Sometimes, averaging projections along the angles is not enough to cancel out the sample-dependent features.  \n",
    "Some low-frequency signal still remains, which may give rings artefacts even after DFF.\n",
    "\n",
    "One possible solution is to apply a high-pass filter on the DFF image, using an unsharp mask with tunable strength `dff_sigma`.\n",
    "\n",
    "\n",
    "```ini\n",
    "# ...\n",
    "[preproc]\n",
    "# Whether to enable the 'double flat-field' filetering for correcting rings artefacts.\n",
    "double_flatfield_enabled = 1\n",
    "# Enable high-pass filtering on double flatfield with this value of 'sigma'\n",
    "dff_sigma = 0.8\n",
    "# ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4641aa52",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Pre-computing the DFF\n",
    "\n",
    "The DFF image can be pre-computed once and for all for the current dataset, with the command `nabu-double-flatfield <dataset>`.\n",
    "\n",
    "A HDF5 file is generated, it contains the DFF image. It can be provided to the `processes_file` parameter of the nabu configuration file:\n",
    "\n",
    "```ini\n",
    "# ...\n",
    "[preproc]\n",
    "processes_file = /path/to/my/dff.h5\n",
    "# ...\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ea39683",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Generate the DFF file: `nabu-double-flatfield /data/projects/tomo-sample-data/training/part4_rings/bamboo_rings.nx /path/to/my/dff.h5`\n",
    "  - This takes a while! To monitor progress, you can use the `--loglevel debug` option.\n",
    "- Edit the nabu configuration file to use \"`dff.h5`\" as a `processes_file`.\n",
    "- Run the reconstruction, visualize the reconstructed slice(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9bbdf03c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2 - Sinogram-based rings removal\n",
    "\n",
    "As of version 2024.1.0, there are three deringers acting on sinogram in nabu:\n",
    "\n",
    "  - Fourier-Wavelets\n",
    "  - Vo\n",
    "  - Mean-subtraction\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe0e3a6c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2.1 - Fourier-Wavelets rings removal\n",
    "\n",
    "This method is an implementation of the paper `[munch09]`, hereby called \"Munch filter\".  \n",
    "It consists in doing a wavelets decomposition of the sinogram, examine the \"vertical coefficients\" to detect vertical lines, and filter them out.\n",
    "\n",
    "```\n",
    "[munch09] B. Munch, P. Trtik, F. Marone, M. Stampanoni, Stripe and ring artifact removal with\n",
    "combined wavelet-Fourier filtering, Optics Express 17(10):8567-8591, 2009.\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc1871f7",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "In Nabu, this rings removal method is enabled using `sino_rings_correction = munch` in `[preproc]`:\n",
    "\n",
    "```ini\n",
    "[preproc]\n",
    "sino_rings_correction = munch\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f944932e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "  - Edit the nabu configuration file to use this method\n",
    "  - Reconstruct and visualize the result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a9eb90b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Advanced parameters\n",
    "\n",
    "The method from paper `[munch09]` has some parameters which can be tuned: \"sigma\" (gaussian filter strength), \"levels\" (number of wavelets decomposition levels), and \"wname\" (name of the wavelet filter).\n",
    "\n",
    "As a rule of thumb:\n",
    "  - Higher values of `sigma` means more filtering\n",
    "  - Higher values of `levels` might yield artefacts, especially in the center (since the filtering occurs accross more wavelets scales, it is more prone to inversion issues)\n",
    "  - Changing the wavelet filter `wname` has less impact\n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15abbc61",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "These advanced options are passed through the `sino_rings_options` parameter in `[preproc]` section. The syntax is the following:\n",
    "\n",
    "```ini\n",
    "[preproc]\n",
    "sino_rings_correction = munch\n",
    "sino_rings_options = sigma=1.0 ; levels=10; wname='db15'; padding=True # mind the semicolon separator\n",
    "```\n",
    "\n",
    "These parameters correspond to  `FW_SIGMA`, `FW_LEVELS` and `FW_WNAME` in PyHST2.\n",
    "\n",
    "Padding can be used in local tomography, where the wavelets filtering can yield edge effects."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "75461cb2",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2.2 - Vo rings removal\n",
    "\n",
    "Nabu ships the rings artefacts removal techniques from Nghia Vo described in\n",
    "\n",
    "> Nghia T. Vo, Robert C. Atwood, and Michael Drakopoulos, \"Superior techniques for eliminating ring artifacts in X-ray micro-tomography,\" Opt. Express 26, 28396-28412 (2018)\n",
    "\n",
    "and more precisely its [tomocupy](https://github.com/tomography/tomocupy) implementation.\n",
    "\n",
    "To enable it in nabu:\n",
    "\n",
    "```ini\n",
    "[preproc]\n",
    "sino_rings_correction = vo\n",
    "```\n",
    "The default options are the following:  \n",
    "`sino_rings_options = snr=3.0; la_size=51; sm_size=21; dim=1`  \n",
    "please refer to algotom `remove_all_stripe()` for their meaning.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ed19c3f7",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 2.3 - Mean subtraction rings removal\n",
    "\n",
    "A very simple deringer, yet often efficient, is to subtract a curve from each line of the sinogram.\n",
    "In the simplest case, this curve can be the mean of all projections.  \n",
    "In short: `sinogram = sinogram - sinogram.mean(axis=0)`.  \n",
    "Usually it's good to high-pass filter this curve to avoid low-frequency effects in the new singoram.\n",
    "\n",
    "To enable it in nabu:\n",
    "\n",
    "```ini\n",
    "sino_rings_correction = mean-subtraction\n",
    "```\n",
    "\n",
    "The default options are the following:  \n",
    "`sino_rings_options = filter_cutoff=(0, 30)`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a518c88a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "Try the different rings removal methods"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3ad6400",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03a798bb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2 - Rings artefacts removal using tomwer\n",
    "\n",
    "The same options are available from the `nabu slice` interface from tomwer ![nabu slice](img/nabu_slice.png)\n",
    "\n",
    "this is available from the `pre processing` stage\n",
    "\n",
    "![tomwer interface for rings corrections](img/tomwer_rings_artefacts.png)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
