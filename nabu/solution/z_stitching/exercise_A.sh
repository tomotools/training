# create the configuration from the .nx file contained in PROCESS_DATA
# warning: there is 3 level of .nx and 4.nx. You must make sure you only add the 3 three one and no duplication
nabu-stitching-config --datasets /data/projects/tomo-sample-data/training/z-stitching/STYLO/PROCESSED_DATA/nxtomos/*_000?.nx

# this creates a stitching.conf file