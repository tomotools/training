{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "379702b6-fada-4e17-ae89-d6bc8a2306dc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Part 5 - volume casting\n",
    "\n",
    "Volume casting allows users to convert any volume created by nabu to another format and or / different values scales.\n",
    "\n",
    "For example, after reconstructing a volume as a 32 bits HDF5, you may want a stack of 16-bits tiff files, automatically re-scaled with the volume min/max values.\n",
    "\n",
    "Volume casting can be done from command line interface (CLI) or from the python API.\n",
    "Complete documentation is available [here](https://tomotools.gitlab-pages.esrf.fr/nabu/cli_tools.html#nabu-cast-apply-volume-casting)\n",
    "\n",
    "We will use volumes in `/data/projects/tomo-sample-data/training/part5_volume_casting/volumes`\n",
    "\n",
    "(old EDF dataset of a pencil, courtesy ID19, 2010)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b81775dc-f8d7-40dd-99e3-4bdb7a7f45d4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "## 1. From command line interface\n",
    "\n",
    "To convert a volume using the command line interface (CLI), use `nabu-cast`.\n",
    "\n",
    "This is a versatile tool which \n",
    "  - Converts **from/to** HDF5, Tiff, Tiff-3D, EDF, jpeg2000\n",
    "  - Supports 32bits, (unsigned) 16bits, (unsigned) 8 bits... \n",
    "  - Automatically rescales values from volume histogram\n",
    "  \n",
    "\n",
    "### CLI Usage\n",
    "\n",
    "Some help and the syntax of volume identifier (url) examples are provided in the nabu-cast help:\n",
    "\n",
    "```bash\n",
    "nabu-cast --help\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bec03cf",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1.1 - Example 1: HDF5 to tiff 16 bits\n",
    "\n",
    "Suppose you have a HDF5 reconstruction and you want to convert it to a stack of tiff images, in 16 bits (`uint16`), rescaling using 10%-90% percentiles.\n",
    "\n",
    "```bash\n",
    "nabu-cast \\\n",
    "  /data/projects/tomo-sample-data/training/part5_volume_casting/volumes/hdf5/5.06_crayon_W150_60_Al2_W0.25_xc1000__vol.hdf5 \\\n",
    "  --output-data-type uint16 \\\n",
    "  --output_volume \"tiff:volume:/path/to/my/tiff_folder\" \\\n",
    "  --rescale_min_percentile \"10%\" \\\n",
    "  --rescale_max_percentile \"90%\"\n",
    "```\n",
    "\n",
    "Note the syntax for providing output files, see [details about volume identifiers (url)](https://tomotools.gitlab-pages.esrf.fr/tomoscan/tutorials/volume.html#identifier)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8c44047-f650-4121-abc1-bd93f27e570e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### 1.2 - Rescaling data\n",
    "\n",
    "Often, in a reconstruction, some values are too low or too high (hot spots, strongly absorbing parts, etc). \n",
    "Converting to a lower dynamic range (ex from float32 to int 16 bits) might deteriorate the overall quality. \n",
    "\n",
    "To avoid this problem, the volume can be \"clipped\" to fixed min/max values during the conversion. \n",
    "\n",
    "In most cases, `nabu-cast` **will need the volume histogram** (to extract min/max values, and/or percentiles).  \n",
    "  - If the histogram is already available along the reconstructed volume, `nabu-cast` will use it.\n",
    "  - Otherwise, `nabu-cast` will re-compute it (slow!).\n",
    "  \n",
    "Therefore, when doing a reconstruction, it's good practice to set `[postproc] output_histogram = 1` in the nabu configuration file - computing histogram *during* reconstruction is fast, while computing it *after* reconstruction is slow (need to reload the entire volume in memory).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0e9e78c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Rescaling using percentiles**\n",
    "\n",
    "A good way to remove too low/high outlier values is to use percentiles: `--rescale_min_percentile` and `rescale_max_percentile`. Default values are 10% and 90%.\n",
    "\n",
    "**Rescaling using fixed min/max values**\n",
    "\n",
    "When rescaling to a very low dynamic range (8 bits) and/or from a reconstruction with two \"big\" histogram modes, it's usually better to use fixed rescaling values: `--data_min` and `--data_max`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7537e547-1968-4208-ae61-45e7668f36e0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    },
    "tags": []
   },
   "source": [
    "### 1.3 - Example 2: EDF-32 bits to EDF-8 bits \n",
    "\n",
    "in this example we will cast a float32 from edf to uint8 and redefining the rescale percentile.\n",
    "\n",
    "``` bash\n",
    "nabu-cast \\\n",
    "  /data/projects/tomo-sample-data/training/part5_volume_casting/volumes/edf/5.06_crayon_W150_60_Al2_W0.25_xc1000__vol/ \\\n",
    "  --output_volume \"edf:volume:/home/lesaint/tomo_training_march_2024/Part5/cast.hdf5?data_path=my_volume\" \\\n",
    "  --rescale_min_percentile 0 \\\n",
    "  --rescale_max_percentile 70\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a52acd47",
   "metadata": {},
   "source": [
    "Note that by default (in the CLI), the volume is cast in `uint16`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "797ae610-8b26-4415-bd58-e4788ba2c083",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2. - Volume casting from Python API\n",
    "\n",
    "The same operation can be done using the python API."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2456a135-be6b-418f-b23d-63b5d5fc7c96",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Here is an example of a volume casting using the python API\n",
    "``` python\n",
    "from tomoscan.esrf.volume import HDF5Volume, TIFFVolume\n",
    "from nabu.io.cast_volume import cast_volume\n",
    "import numpy \n",
    "\n",
    "input_volume = HDF5Volume(\n",
    "    file_path=\"5.06_crayon_W150_60_Al2_W0.25_xc1000__vol.hdf5\",\n",
    "    data_path=\"entry/reconstruction\"\n",
    ")\n",
    "output_volume = TIFFVolume(\n",
    "    folder=\"output_vol\",\n",
    "    volume_basename=\"5.06_crayon_W150_60_Al2_W0.25_xc1000\",\n",
    ")\n",
    "\n",
    "output_volume.overwrite = True\n",
    "cast_volume(\n",
    "    input_volume=input_volume,\n",
    "    output_volume=output_volume,\n",
    "    output_data_type=numpy.uint16,\n",
    "    rescale_min_percentile=0,\n",
    "    rescale_max_percentile=100,\n",
    ")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de1fe661-e261-4b71-8b63-f30356348a46",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Note 1: in the python API, the `output-data-type` is a mandatory parameter (no default value is set).\n",
    "\n",
    "Note 2 on volumes and python: if you want to \"play\" and or / know more about tomoscan volumes [this can be a nice entry point](https://tomotools.gitlab-pages.esrf.fr/tomoscan/tutorials/volume.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ed35bd5",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
