{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a73400a5",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Part 2: finding the center of rotation\n",
    "\n",
    "Finding the CoR is a crucial step in reconstruction. This parameter is probably the most important - without a correct center of rotation, no reconstruction can be done.\n",
    "\n",
    "Estimating the CoR can be done manually or (almost) automatically.\n",
    "\n",
    "In this part, we see how to\n",
    "\n",
    "  - Review different CoR estimation methods available with Nabu\n",
    "  - Use nabu (command line) to find the CoR\n",
    "  - Use tomwer (graphical interface) to find the CoR\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9f4cbf33",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "## 1 - Center of rotation estimation with Nabu\n",
    "\n",
    "Nabu provides several Center of Rotation (CoR) estimation methods.\n",
    "\n",
    "See the [documentation page](https://www.silx.org/pub/nabu/doc/estimation.html#center-of-rotation) for CoR.\n",
    "\n",
    "For this tutorial, we still use the half-acquisition \"bamboo\" dataset. \n",
    "\n",
    "You can generate a configuration file with `nabu-config --dataset /where/you/generated/the/bambou_hercules.nx`.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e2705e88",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1.1 - Where to provide center of rotation (estimation) in configuration file\n",
    "\n",
    "The relevant section in configuration file is `[reconstruction]`.\n",
    "\n",
    "```ini\n",
    "[reconstruction]\n",
    "rotation_axis_position = <cor_or_method>\n",
    "```\n",
    "\n",
    "If a scalar value is provided (e.g. `2069.5`), then this value will be used for reconstruction.\n",
    "\n",
    "Otherwise, the parameter is the name of an estimation method (eg. `rotation_axis_position = sliding-window`).\n",
    "\n",
    "**Note** an empty value means the center is half the detector width. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0812209",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1.2 List of estimation methods\n",
    "\n",
    "Nabu comes with a collection of CoR estimation methods. They work differently (on pairs of radios, sinograms, with correlation or subtraction, etc).\n",
    "\n",
    "\n",
    "| **Method name**          | **Notes**                                                               |\n",
    "|--------------------------|-------------------------------------------------------------------------|\n",
    "| sliding-window           | Fast, works well for micro-CT                                     |\n",
    "| growing-window           | Slow, global search                                                     |\n",
    "| composite-coarse-to-fine | Slow, uses an initial estimate. Equivalent to \"near\" in octave-fasttomo |\n",
    "| fourier-angles           | Fast, needs initial estimate                                            |\n",
    "| octave-accurate          | Fast, only works for centered tomography                                |\n",
    "| sino-sliding-window      | Same principle as sliding-window, applied on sinogram                   |\n",
    "| sino-growing-window      | Same principle as growing-window, applied on sinogram                   |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16cdaddf",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Older, less reliable methods\n",
    "\n",
    "| **Method name**     | **Notes**                                             |\n",
    "|---------------------|-------------------------------------------------------|\n",
    "| global              | Very slow, global search. Works well in nano-CT.      |\n",
    "| centered            | Fast, works for centered tomo.                        |\n",
    "| sino-coarse-to-fine | Fast, sinogram-based. Needs a 360 degrees scans.      |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd3c51e1",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1.3 - Estimators relying on an initial estimate\n",
    "\n",
    "ESRF Bliss acquisition files come with an initial estimate of the center of rotation. \n",
    "This CoR estimate is done when initially aligning the beamline setup.\n",
    "\n",
    "This value can be obtained by dividing the \"yrot\" motor position (usually `instrument/positioners/yrot` in Bliss files) with the pixel size.\n",
    "\n",
    "In the NX file, this information can be found in `instrument/detector/estimated_cor_from_motor`, in pixel units, offset from the middle of the detector.\n",
    "\n",
    "By default, **nabu will automatically look in the NX file** for the `estimated_cor_from_motor` field.\n",
    "\n",
    "**Caution**\n",
    "  - In the NX file, `estimated_cor_from_motor` is a position (in pixel units) from middle of the detector\n",
    "  - In nabu configuration `rotation_axis_position` is a position (in pixel units) from the left-most pixel of the detector"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55ef546b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "\n",
    "The following estimators **should** use an initial estimate - they work well, but will have poor performance without a good-enough initial estimate:\n",
    "  - `composite-coarse-to-fine` (aka \"near\")\n",
    "  - `fourier-angles` \n",
    "\n",
    "The following estimators **can** use an initial estimate - they still have decent performance if not:\n",
    "  - `sliding-window` (and `sino-sliding-window`)\n",
    "\n",
    "All the other estimators **do not** use an initial estimate."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8da2ce7d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "  - Use `sliding-window` on the \"bamboo\" dataset - it should correctly find the CoR.\n",
    "  - Set `rotation_axis_position=2069.5` (this will force `nabu` to use this value), run the reconstruction of your favourite slice and compare with previously."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23300004",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### 1.4 - Advanced options\n",
    "\n",
    "Most estimators can be tuned with advanced options. In nabu, the default options try to be good for most cases.\n",
    "\n",
    "In the configuration file, these options are provided in the field `cor_options`.\n",
    "\n",
    "For example, to use `sliding-window` and look to the right side of the detector:\n",
    "\n",
    "```ini\n",
    "[reconstruction]\n",
    "rotation_axis_position = `sliding-window`\n",
    "cor_options = side=\"right\"\n",
    "```\n",
    "\n",
    "The syntax of `cor_options` is admittedly cumbersome:\n",
    "  - Don't forget quotes for side=\"right\"\n",
    "  - Parameters are separated by semicolon: `side=\"right\"; low_pass=1; high_pass=20`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e08113a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Set the config file as below and run the reconstruction.\n",
    "```ini\n",
    "[reconstruction]\n",
    "rotation_axis_position = `sliding-window`\n",
    "cor_options = side=2069.5\n",
    "```\n",
    "- Compare the obtained COR value obtained. In this case, `nabu` passes that value to the `sliding-window` algo as a first guess."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6c79778",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.5 - Reconstructing one slice with several COR values at once\n",
    "\n",
    "One can also reconstruct one slice at several COR positions at once with\n",
    "\n",
    "```bash\n",
    "nabu-multicor nabu.conf middle 2049:2069:1\n",
    "```\n",
    "This will reconstruct the middle slice at all COR values from 2049 to 2069, with a step of 1 pixel. All the other options are taken from the `nabu.conf` file.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27709637",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4169d867",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2 - Using tomwer \"semi-automatic\" rotation axis finder\n",
    "\n",
    "See the dedicated notebook"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
