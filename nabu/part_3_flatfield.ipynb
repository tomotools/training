{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "a73400a5",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Part 3: flat-field normalization\n",
    "\n",
    "Flat-field normalization of the radios is usually the fist step of the processing chain.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c80dde48",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1. What is Flat-field normalization\n",
    "\n",
    "Flat-field is a normalization step which \"corrects\" for the incoming beam intensity.\n",
    "\n",
    "Remember that in absorption tomography (in the ideal monochromatic beam setting), the beam intensity decays through the sample between positions $[x_0, x]$ as a Lambert-Beer law:\n",
    "\n",
    "$$\n",
    "I(x) = I_0 \\exp \\left( -\\displaystyle\\int_{x_0}^x \\mu(l) \\operatorname{d}\\! l \\right)\n",
    "$$\n",
    "\n",
    "and we aim at \"reconstructing\" the linear absorption coefficient $\\mu(x)$ from line integrals:\n",
    "\n",
    "$$\n",
    "-\\ln\\left(\\dfrac{I(x)}{I_0} \\right) = \\displaystyle\\int_{x_0}^x \\mu(l) \\operatorname{d}\\!l\n",
    "$$\n",
    "\n",
    "(the same kind of equation holds for phase-contrast tomography).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91ca9747",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "$$\n",
    "-\\ln\\left(\\dfrac{I(x)}{I_0} \\right) = \\displaystyle\\int_{x_0}^x \\mu(l) \\operatorname{d}\\!l\n",
    "$$\n",
    "\n",
    "$I_0 = I(x=0)$ is the incoming beam intensity, which has to be recorded alongside the \"projections\" $I(x)$.\n",
    "\n",
    "As detectors are not ideal, they record a background noise (\"dark current\") even without beam. This dark image has to be subtracted from the incoming intensity.\n",
    "\n",
    "$$\n",
    "\\dfrac{I}{I_0} = \\dfrac{P - D}{F - D}\n",
    "$$\n",
    "where $P$ is the projection image recorded on detector, $D$ is the dark image, and $F$ is the flat image ($F = I_0 + D$)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a3933d0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 2. How Flat-field normalization is done\n",
    "\n",
    "The normalized intensity at index $k$, $I_n (k)$ is given by\n",
    "\n",
    "$$\n",
    "I_n (k) = \\displaystyle\\dfrac\n",
    "{\n",
    "  I (k) - D \n",
    "}\n",
    "{\n",
    "  F (k) - D\n",
    "}\n",
    "$$\n",
    "\n",
    "where $I (k)$ is the raw projection number $k$, $F (k)$ is the flat corresponding to radio $k$, and $D$ is the detector dark-current.\n",
    "\n",
    "For practical reasons, it's not possible to record one flat image (and one dark image) for every projection.  \n",
    "Usually there is not as many flats as radios, so $F (k)$ is obtained by **linear interpolation**:\n",
    "\n",
    "```\n",
    "     F(k1)    I(k)    F(k2)\n",
    "------|--------|-------|------ ...\n",
    "\n",
    "```\n",
    "\n",
    "$$\n",
    "F(k) = \\dfrac{k_2 - k}{k_2 - k_1} F_{k_1} + \\dfrac{k - k_1}{k_2 - k_1} F_{k_2}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5802a29",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Usually, in a tomo acquisition, the acquisition scheme is the following:\n",
    "  - Darks\n",
    "  - Flats\n",
    "  - Projections\n",
    "  - (optionally) Flats"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "600249c3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 3. \"Reduced\" flats and darks\n",
    "\n",
    "When doing a scan, **series** of flats/darks are acquired.\n",
    "\n",
    "Example sequence:\n",
    "  - Acquire 50 darks\n",
    "  - Acquire 100 flats\n",
    "  - Acquire 10 000 projections\n",
    "  - Acquire 100 flats\n",
    "  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb7511fb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Each series of flats/darks is **reduced** to obtain **one** image (per sequence).  \n",
    "In the previous example:\n",
    "\n",
    "  - 50 darks ------> mean ---> one dark image    \n",
    "  - 100 flats -----> median ---> one flat image  \n",
    "  - 10 000 projs\n",
    "  - 100 flats -----> median ---> one flat image\n",
    "  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16d9ee19",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "In the previous acquisition/processing system (octave-fasttomo), the resulting files were called `dark0000.edf`, `refHST<num>.edf` \n",
    "\n",
    "In nabu/tomwer, the reduced flats/darks are called `<dataset_prefix>_darks.h5` and `<dataset_prefix>_flats.h5`\n",
    "\n",
    "**These final (reduced) frames are used for flat-field normalization.**"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9041609a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Example (1/5)**\n",
    "\n",
    "Principle:\n",
    "\n",
    "![image.png](img/flats05b.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4ad5e02",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Example (2/5)**\n",
    "\n",
    "Bliss file layout\n",
    "\n",
    "![image.png](img/flats01.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a43ed8d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Example (3/5)**\n",
    "\n",
    "Bliss file layout with data\n",
    "\n",
    "![image.png](img/flats02.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d504a5e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Example (4/5)**\n",
    "\n",
    "NXTomo file layout\n",
    "\n",
    "![image.png](img/flats03.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11a2a998",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Example (5/5)**\n",
    "\n",
    "`image_key_control` field in NXTomo file\n",
    "\n",
    "![image.png](img/image_key.png)\n",
    "\n",
    "In the NXTomo convention:\n",
    "  - 2 = dark\n",
    "  - 1 = flat\n",
    "  - 0 = projection\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5abcb4d3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### How nabu does the flats/darks reduction\n",
    "\n",
    "Before computing the mean/median of darks/flats, nabu/tomwer will first attempt to load them (to avoid re-computations).\n",
    "\n",
    "Nabu will\n",
    "  - Try to load from `darks_flats_dir` (in `[preproc]` of the configuration file), if specified\n",
    "  - Try to load the `<dataset>_darks.h5` and `<dataset>_flats.h5` files if they are alongside the dataset\n",
    "  - Try to load them from the output reconstruction directory"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "544cbd1e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 4. Possible flat-field modes with nabu\n",
    "\n",
    "In the configuration file (section `[preproc]`), the paramter `flatfield` can have these possible values:\n",
    "\n",
    "  - `flatfield = 1` or True (default): do flat-field normalization with the frames from the dataset  \n",
    "  - `flatfield = 0` or False: no flat-field normalization\n",
    "  - `flatfield = force-load`: perform flatfield regardless of the dataset by attempting to load darks/flats\n",
    "  - `flatfield = force-compute`: perform flatfield, ignore all .h5 files containing already computed darks/flats\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2b041b8",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 5. Synchrotron current normalization\n",
    "\n",
    "Sometimes a beam refill happens during an acquisition.  \n",
    "Depending on how flats are acquired, flat-fielding might not correct for the incoming beam intensity jump.  \n",
    "This is especially detrimental for half-tomography, as projections at $\\theta$ and $\\theta + 180°$ will not have the same incoming intensity.\n",
    "\n",
    "![refill_sinogram](img/beam_refill.png)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0966115f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "When information on synchrotron current (SRCurrent) is available, it's possible to normalize each with this current, according to\n",
    "\n",
    "$$\n",
    "P' = \\dfrac{P - D}{s/s_{Max}} + D\n",
    "$$\n",
    "\n",
    "where $s$ is the synchrotron current for the current frame, $s_{Max}$ is the max current, and $D$ is the dark frame.\n",
    "\n",
    "In nabu, the current normalization is done during flat-field:\n",
    "\n",
    "$$\n",
    "\\dfrac{P' - D}{F' - D} = \\dfrac{(P - D)/s_P}{(F - D)/s_F}\n",
    "$$\n",
    "\n",
    "\n",
    "To enable it:\n",
    "\n",
    "```ini\n",
    "[preproc]\n",
    "# ...\n",
    "normalize_srcurrent = 1\n",
    "# ...\n",
    "```\n",
    "\n",
    "Since version 2024.1.0, SRCurrent normalization is enabled by default."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
