{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9478b64a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Advanced Nabu usage\n",
    "\n",
    "\n",
    "  - Using darks/flats from another dataset\n",
    "  - CTF pipeline (ID16A, ID16B)\n",
    "  - Excluding projections from reconstruction\n",
    "  - Binning\n",
    "  - Reconstructing from even/odd projections\n",
    "  - Using nabu through the Python API"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bfda7c5e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Using darks/flats from another dataset\n",
    "\n",
    "Sometimes for practical reasons, you want to use flats/darks from another dataset (or even custom darks/flats).  For example:\n",
    "  - Flats/darks could not be acquired during the scan\n",
    "  - The sample is in a container whose contribution should be \"removed\" from the main signal\n",
    "  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5bc4f455",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "For the record (see \"Part 3 - Flat-field normalization\"): \n",
    "  - Nabu will attempt at loading darks/flats named `{dataset_prefix}_darks.h5` and `{dataset_prefix}_flats.h5`\n",
    "  - If not present, nabu will compute the reduced flats/darks\n",
    "\n",
    "In this case we want to force nabu to use provided flats/darks.\n",
    "\n",
    "To do so, **simply replace the files** `{dataset_prefix}_darks.h5` and `{dataset_prefix}_flats.h5` with your custom darks/flats.\n",
    "\n",
    "Note that creating HDF5/NXTomo files from scratch can be tedious. You can use `nxtomo`, see for example https://nxtomo.readthedocs.io/en/latest/tutorials/create_from_scratch.html"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f5276cc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## CTF pipeline\n",
    "\n",
    "Contrast Transfer Function (CTF) is another phase retrieval method.  \n",
    "Nabu implements a single-distance CTF method, possibly accounting for conicity.\n",
    "\n",
    "Note that in this case, the overall processing is usually slower, as reconstruction cannot be done simply by sub-volumes.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62bef896",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The usual parameters for ID16A and ID16B are the following:\n",
    "\n",
    "```ini\n",
    "[preproc]\n",
    "flatfield = 1\n",
    "flat_distortion_correction_enabled = 0\n",
    "flat_distortion_params = tile_size=100; interpolation_kind='linear'; padding_mode='edge'\n",
    "take_logarithm = 0\n",
    "ccd_filter_enabled = 1\n",
    "ccd_filter_threshold = 0.04\n",
    "\n",
    "[phase]\n",
    "method = ctf\n",
    "delta_beta = 80\n",
    "ctf_geometry = z1_v=None; z1_h=None; detec_pixel_size=None; magnification=True\n",
    "ctf_advanced_params = length_scale=1e-5; lim1=1e-5; lim2=0.2; normalize_by_mean=True\n",
    "\n",
    "[reconstruction]\n",
    "rotation_axis_position = global\n",
    "cor_options =\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd9d774e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Wavefront \"distortion\" correction**\n",
    "\n",
    "Nano-tomography beamlines use a very structured wavefront.  \n",
    "The projection images have to be aligned onto a reference (empty beam, i.e flat) image before flat-field normalization and phase retrieval.\n",
    "\n",
    "In the configuration file:\n",
    "\n",
    "```ini\n",
    "[preproc]\n",
    "flat_distortion_correction_enabled = 1\n",
    "flat_distortion_params = tile_size=100; interpolation_kind='linear'; padding_mode='edge'\n",
    "```\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3235be1",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Translation movements**\n",
    "\n",
    "Step-by-step scans can be done with \"random\" motions to avoid rings artefacts. The motors displacements are recorded in a text file, where each line has two values for the horizontal and vertical displacements (in pixel units).\n",
    "\n",
    "In the configuration file:\n",
    "\n",
    "```ini\n",
    "[reconstruction]\n",
    "translation_movements_file = correct.txt\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f42919b7",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Excluding projections from reconstruction\n",
    "\n",
    "Sometimes some projections, or even a whole angular range, have to be discarded from reconstruction.\n",
    "\n",
    "In the configuration file, there is a `exclude_projection` parameter in `[dataset]`, which can be used in three ways:\n",
    "\n",
    "  - `exclude_projections = indices = exclude_indices.txt`: Path to a text file with one integer per line. Each corresponding projection INDEX will be ignored. This is the former default.\n",
    "  - `exclude_projections = angles = exclude_angles.txt` : Path to a text file with angle in DEGREES, one per line. The corresponding angles will be ignored.\n",
    "  - `exclude_projections = angular_range = [a, b]` ignore angles belonging to angular range [a, b] in degrees, with b included.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb8b60d3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Reconstruction with binning\n",
    "\n",
    "Binning is an operation consisting in averaging the contribution of several pixels, to form one equivalent pixel.  \n",
    "For example, in a 2X2 binning of an image, each output pixel is the average of 4 adjacent pixels.\n",
    "\n",
    "Nabu allows to perform binning in both dimensions:\n",
    "  - Horizontal binning: the horizontal dimension is reduced (number of detector columns). **Each output slice will be smaller**, eg. 1024x1024 instead of 2048x2048.\n",
    "  - Vertical binning: the vertical dimension is reduced (number of detector rows). **There will be less output slices**, ex. 1080 horizontal slices instead of 2160 horizontal slices.\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4361552",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "In the configuration file:\n",
    "\n",
    "```ini\n",
    "# Binning factor in the horizontal dimension when reading the data.\n",
    "# The final slices dimensions will be divided by this factor.\n",
    "binning = 2\n",
    "# Binning factor in the vertical dimension when reading the data.\n",
    "# This results in a lesser number of reconstructed slices.\n",
    "binning_z = 2\n",
    "```\n",
    "\n",
    "(nabu allows any integer number).\n",
    "\n",
    "Note that **only the reconstruction step will be faster**, as data still has to be loaded to perform pixels averaging."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39150481",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Projections subsampling**\n",
    "\n",
    "It's also possible to reconstruct a volume using only one projection out of $N$.\n",
    "\n",
    "In the configuration file, the parameter can be \n",
    "  - an integer: take 1 projection out of N\n",
    "  - in the form or N:M : take 1 projection out of N, start from projection M\n",
    "\n",
    "```ini\n",
    "projections_subsampling = 2\n",
    "```\n",
    "\n",
    "Note that in the current HDF5 library implementation, reading one image out of N is not faster (actually slower !!).  \n",
    "This means that **subsampling has no performance benefit when using HDF5**.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd087aad",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Reconstructing from even/odd projections\n",
    "\n",
    "In some cases it can be useful to reconstruct the data from only even-numbered or odd-numbered projections.\n",
    "For example the denoiser `noise2inverse` needs to split the (reconstructed) data.\n",
    "\n",
    "  - To reconstruct from only the even projections, use `projections_subsampling = 2` or `projections_subsampling = 2:0`.\n",
    "  - To reconstruct from only the odd projections, use `projections_subsampling = 2:1`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "442828c1",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "619eb9e1",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Using Nabu API\n",
    "\n",
    "This is an overview of Nabu API usage.  \n",
    "For a comprehensive API documentation, please refer to the [API reference](https://www.silx.org/pub/nabu/doc/apidoc/nabu.html)\n",
    "\n",
    "Nabu provides building blocks that aim at being easy to use:\n",
    "  - Input parameters are numpy arrays\n",
    "  - Each function/class has a clear and decoupled role"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e3b8278",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Reconstruction\n",
    "\n",
    "A frequent use of nabu API is to simply reconstruct a sinogram.\n",
    "\n",
    "```python\n",
    "import numpy as np\n",
    "from nabu.testutils import get_data\n",
    "from nabu.reconstruction.fbp import Backprojector\n",
    "\n",
    "sino = get_data(\"mri_sino500.npz\")[\"data\"]\n",
    "# Backprojection operator\n",
    "B = Backprojector(sino.shape)\n",
    "rec = B.fbp(sino)\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f648d20f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Phase retrieval\n",
    "\n",
    "```python\n",
    "from nabu.preproc.phase import PaganinPhaseRetrieval\n",
    "\n",
    "P = PaganinPhaseRetrieval(\n",
    "    radio.shape,\n",
    "    distance=0.5, # meters\n",
    "    energy=20, # keV\n",
    "    delta_beta=50.0,\n",
    "    pixel_size=1e-06, # meters\n",
    ")\n",
    "radio_phase = P.apply_filter(radio)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03dc940b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Pipeline processing from a configuration file (from within Python !)\n",
    "\n",
    "```python\n",
    "from nabu.pipeline.fullfield.processconfig import ProcessConfig\n",
    "from nabu.pipeline.fullfield.reconstruction import FullFieldReconstructor\n",
    "\n",
    "# Parse the configuration file, build the processing steps/options\n",
    "conf = ProcessConfig(\"/path/to/nabu.conf\")\n",
    "\n",
    "# Spawn a \"reconstructor\"\n",
    "R = FullFieldReconstructor(conf)\n",
    "print(R.backend) # cuda or numpy\n",
    "```\n",
    "\n",
    "```python\n",
    "R._print_tasks() # preview how the volume will be reconstructed\n",
    "\n",
    "# Launch reconstruction\n",
    "R.reconstruct()\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90450845",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
