{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "09e042f4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Part 1: basic reconstruction\n",
    "\n",
    "In this part, we see how to\n",
    "\n",
    "  - Get started with Nabu configuration files\n",
    "  - Perform a basic reconstruction\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bee733e3",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b64e9b78",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1. Reconstruction using nabu\n",
    "\n",
    "In this part, we use the command line (nabu + nxtomomill) to perform a basic reconstruction.\n",
    "\n",
    "There are roughly three steps:\n",
    "  - Convert data (Bliss-HDF5 or Spec-EDF) to Nexus : `nxtomomill`\n",
    "  - Create and edit a nabu confiration file : `nabu-config`\n",
    "  - Run the reconstruction : `nabu` "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63f075b7",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.1 - Converting a Bliss HDF5 acquisition scan to NX\n",
    "\n",
    "At ESRF, all tomography beamlines (except ID16A and ID16B) are using the Bliss acquisition system, which produces HDF5 files. \n",
    "\n",
    "Using HDF5 format instead of EDF was a step forward, but there are still a few drawbacks:\n",
    "  - These HDF5 files are beamline-dependent (eg. fscan vs EBSTomo)\n",
    "  - The structure changes over time, has missing fields/units ; which can lead to crashes\n",
    "    - Format is now much more stable as of 2024\n",
    "\n",
    "On the other hand, nabu/tomwer need a stable and standard file format.\n",
    "  - The [Nexus format](https://manual.nexusformat.org) was chosen\n",
    "  - Need a tool to convert HDF5 from Bliss layout to Nexus layout\n",
    "  - This conversion **does not** duplicate data - only re-arange the internal layout\n",
    "  \n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83f17900",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "For this first part, we will be using the dataset:\n",
    "\n",
    "`/data/projects/tomo-sample-data/training/part1_basic_reco/bambou_hercules_0001.h5`\n",
    "\n",
    "(scan of a Bamboo stick, courtesy Ludovic Broche, ID19)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08de79e0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on: convert Bliss-H5 to Nexus\n",
    "\n",
    "Use the command line `nxtomomill` to convert between (Bliss) HDF5 and NX layout.\n",
    "\n",
    "`nxtomomill h52nx /data/projects/tomo-sample-data/training/part1_basic_reco/bambou_hercules_0001.h5  bambou_hercules_0001.nx --single-file\n",
    "`\n",
    "\n",
    "**Note** make sure that you have write access to the directory containing the target `.nx` file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a87129f",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.1- (alt.)  Converting a Spec acquisition to NX\n",
    "\n",
    "As of 2024, nano-tomography beamlines ID16A and ID16B use the SPEC acquisition system.\n",
    "\n",
    "While the reconstruction software (nabu/tomwer) natively handles EDF datasets, the ESRF filesystem (GPFS) makes it very slow. \n",
    "\n",
    "It is therefore advised to first convert the EDF dataset to NX. By default **this duplicates the data**.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6082268",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "We will use the dataset\n",
    "\n",
    "`/data/projects/tomo-sample-data/training/part1_basic_reco/gut_ctrl_7_mid_ovw1_150nm_1_` \n",
    "\n",
    "(courtesy Alexandra Pacureanu, ID16A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c7224fc",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on: convert Spec-EDF to Nexus\n",
    "\n",
    "Use the command `nxtomomill` to convert thre EDF dataset to Nexus:\n",
    "\n",
    "`\n",
    "nxtomomill edf2nx /data/projects/tomo-sample-data/training/part1_basic_reco/gut_ctrl_7_mid_ovw1_150nm_1_ gut_ctrl_7_mid_ovw1_150nm_1_.nx\n",
    "`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6124d345",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.2 - Generating a configuration file\n",
    "\n",
    "At this stage we have a `.nx` file which \"contains\" the data, and necessary metadata to reconstruct it.\n",
    "\n",
    "To create a configuration file from scratch, the command-line tool `nabu-config` can be used:\n",
    "\n",
    "```bash\n",
    "nabu-config\n",
    "```\n",
    "\n",
    "this creates a file (`nabu.conf` by default) with pre-filled parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d49b88f5",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "To choose the file name:\n",
    "\n",
    "```bash\n",
    "nabu-config --output my_config.conf\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30b6d446",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "By default, only a subset of the options are visible in the configuration file.\n",
    "\n",
    "To see all the parameters:  \n",
    "```bash\n",
    "nabu-config --level advanced\n",
    "```\n",
    "\n",
    "to retain only a handful of parameters:\n",
    "```bash\n",
    "nabu-config --level required\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8919768",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on\n",
    "\n",
    "- Create a directory which will contain the nabu configuration file, and possibly the reconstructions.\n",
    "- Create a nabu configuration file from scratch: `nabu-config --dataset <path to the NX file>`\n",
    "- Open the generated file and inspect the default parameters\n",
    "- Specify the output file/directory in the [output] section (WARNING : if not set, `nabu` will write the output file(s) to the direcory which contains the NX file.)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4e681e9",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.3 - Basic reconstruction\n",
    "\n",
    "Let's run our first reconstruction, using the NX file produced by nxtomomill.\n",
    "\n",
    "\n",
    "The syntax is `nabu nabu.conf [parameters]`.  \n",
    "\n",
    "By default, `nabu nabu.conf` will reconstruct the whole volume, which can take some time."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42e10c51",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Reconstructing one slice\n",
    "\n",
    "Usually, to quickly assess the reconstruction parameters, only one or several slices are reconstructed.\n",
    "\n",
    "\n",
    "To reconstruct only the middle slice:  \n",
    "\n",
    "```bash\n",
    "nabu nabu.conf --slice middle\n",
    "```\n",
    "\n",
    "To reconstruct the first and last slices:\n",
    "\n",
    "```bash\n",
    "nabu nabu.conf --slice first\n",
    "nabu nabu.conf --slice last\n",
    "```\n",
    "\n",
    "To reconstruct a limited range of slices:\n",
    "```bash\n",
    "nabu nabu.conf --slice <first>-<end>\n",
    "```\n",
    "\n",
    "See also: [command-line interface](https://www.silx.org/pub/nabu/doc/nabu_cli.html) to modify `start_xyz` and `end_xyz` in the configuration file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "219cc16e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on: Basic reconstruction\n",
    "\n",
    "  - Reconstruct one or several slices of your choice\n",
    "  - Examine the result\n",
    "  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e6ead38",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![bamboo reconstruction](img/bamboo_rec1.png)\n",
    "\n",
    "In this case:  \n",
    "  - No phase retrieval means a rather noisy reconstruction\n",
    "  - Half acquisition yields a 4120x4100 slice\n",
    "  - The (default) CoR estimation method `sliding-window` works well"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49ae660a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on: enable phase\n",
    "\n",
    "  - Enable phase retrieval (Paganin) and re-run the reconstruction\n",
    "  \n",
    "![bamboo reconstruction with phase retrieval](img/bamboo_rec2.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a40441a7",
   "metadata": {
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "source": [
    "You can use [`silx compare <file1> <file2>`](https://www.silx.org/doc/silx/latest/applications/compare.html) (available in `silx 2.0`) to compare the two or more files.\n",
    "\n",
    "Examples:\n",
    "- To compare two `tiff` files: `silx compare file1.tiff file2.tiff`\n",
    "- To compare two `hdf5` files : `silx compare \"silx:bambou_hercules_0001_rec_2064.000_01080.hdf5?path=/entry0000/reconstruction/results/data&slice=0\" \"silx:bambou_hercules_0001_rec_2055.000_01080.hdf5?path=/entry0000/reconstruction/results/data&slice=0\"` (of course, `path=` and file names will have to be adapted to your use case).\n",
    "- To compare a list of files whose names share a pattern: `silx compare \"silx:bambou_hercules_0001_rec_2*_01080.hdf5?path=/entry0000/reconstruction/results/data&slice=0\"`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a36cc154",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.4 - Using checkpoints\n",
    "\n",
    "Sometimes it's useful to save partially-processed data in the middle of the process, for debugging/visualization.\n",
    "\n",
    "Nabu enables to \n",
    "  - Dump data from arbitrary point in the processing pipeline (\"checkpoint\")\n",
    "  - Resume the processing from this data\n",
    "  \n",
    "  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc197c26",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Creating checkpoints\n",
    "\n",
    "\n",
    "The relevant section in configuration file is `[pipeline]`:\n",
    "\n",
    "```ini\n",
    "[pipeline]\n",
    "# Example: dump data after phase retrieval and sinogram construction\n",
    "save_steps = phase, sinogram\n",
    "```\n",
    "\n",
    "**Note** checkpoints can be saved in a custom file with the `steps_file` parameter."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c101b86e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on (optional)\n",
    "\n",
    "  - Save the data after phase retrieval and sinogram build"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5afa0a3b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Resuming from a checkpoint\n",
    "\n",
    "The parameter name is `resume_from_step`.  \n",
    "You can save any number of step (`flatfield`, `phase`, `sinogram`, ...), but the processing can be resumed from only one checkpoint.\n",
    "\n",
    "```ini\n",
    "[pipeline]\n",
    "save_steps = sinogram\n",
    "resume_from_step = sinogram\n",
    "```\n",
    "\n",
    "This particular configuration is useful for manually finding the center of rotation (many trial-and-error reconstructions needed).\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13de3bb4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on: resuming from checkpoints\n",
    "\n",
    "  - Configure nabu to dump the sinograms, and to resume the processing from already-saved sinograms\n",
    "    - `save_steps = sinogram` and `resume_from_step = sinogram`\n",
    "  - Reconstruct the **middle** slice: `nabu nabu.conf --slice middle`\n",
    "  - Reconstruct the **last** slice: `nabu nabu.conf --slice last`\n",
    "  - What do you observe ?\n",
    "  - Reconstruct again the **last** slice: `nabu nabu.conf --slice last`\n",
    "  \n",
    "  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "125928bb",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "---"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
