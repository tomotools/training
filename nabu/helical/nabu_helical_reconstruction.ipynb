{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "09e042f4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Tomo-tools training - Part 1: basic helical reconstruction\n",
    "\n",
    "In this part, we see how to\n",
    "\n",
    "  - Connect to ESRF compute infrastructure and activate the tomo-tools software\n",
    "  - Get started with nabu-helical configuration files\n",
    "  - Perform a basic helical reconstruction\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f327515",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Activating the tomo-tools software\n",
    "\n",
    "Nabu-helical is part of Nabu, the activation process is the same - you have to enter a command to use it:\n",
    "\n",
    "```bash\n",
    "source /scisoft/tomotools/activate training_march_2023\n",
    "```\n",
    "\n",
    "The integration of Nabu-helical into Nabu exploits python and pycuda, on which Nabu is based.\n",
    "\n",
    "With the goal of dealing with very large datasets some python classes of nabu-helical have been left, by design, on the CPU. This is done in order to fully exploit the host RAM $\\simeq N ~\\times~Tb$. The backprojection in particular remains on the GPU, its memory footprint remains limited by the fact that it is applied slice by slice.\n",
    "Other preprocessing step instead, are performed on the CPU. \n",
    "\n",
    "To improve the performances some selected preprocessing classes have been translated to c++. They maintain the same API and can be seamlessy plugged into the master version.  You can test this boosting by activating the following environment:\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a7841c6",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "\n",
    "```bash\n",
    "source /scisoft/tomotools/activate bm18_training_march_2023\n",
    "```\n",
    "Nothing changes except the speed.\n",
    "\n",
    "#### Notice:\n",
    "\n",
    "for maximal execution speed in the *c++* accelerated parts you will profit from having required many cores. This is done with the `--cpus-per-task` argument in the allocation request.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b64e9b78",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1. Reconstruction using nabu-helical\n",
    "\n",
    "In this part, we use the command line (nabu + nxtomomill) to perform a basic reconstruction.\n",
    "\n",
    "There are roughly three steps:\n",
    "  - Convert data (Bliss-HDF5 Spec-EDF) to Nexus : `nxtomomill`\n",
    "  - Create and edit a nabu configuration file : `nabu-config`\n",
    "  - Run the reconstruction : `nabu-helical` "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63f075b7",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.1 - Converting a Bliss HDF5 acquisition scan to NX\n",
    "\n",
    "For a basic helical scan the conversion to NX format is done in the same way as for the non-helical case.\n",
    "We will see more advanced command to convert z-stages into an helical scan.\n",
    "For this first part, we will be using the dataset: `/scisoft/tomo_training/helica/HA-700_23.32um_apple_helical_0001` (scan of an apple, courtesy of Paul Tafforeau and Benoit Cordonnier, taken at bm18)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08de79e0",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Hands-on: convert Bliss-H5 to Nexus\n",
    "\n",
    "Use the command line `nxtomomill` to convert between (Bliss) HDF5 and NX layout. \n",
    "In the few lines below,  to mantain a compact form of the command line, we use the variable *tdd* for the tomography dataset directory, and *sn* for the sample name:\n",
    "\n",
    "```bash\n",
    "tdd=/scisoft/tomo_training/helical/apple\n",
    "sn=HA-700_23.32um_apple_helical_0001\n",
    "nxtomomill h52nx $tdd/$sn/${sn}.h5 $sn.nx\n",
    "\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6124d345",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.2 - Generating a configuration file\n",
    "\n",
    "To create a configuration file from scratch, the command-line tool `nabu-config` can be used:\n",
    "\n",
    "```bash\n",
    "nabu-config --helical 1 --output apple.conf --dataset $sn.nx\n",
    "```\n",
    "\n",
    "this creates a file `apple.conf`  (`nabu.conf` would be the default) with pre-filled  dataset location ( defaults to an empty slot)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30b6d446",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "To see all the parameters add `--level advanced` while for a minimalistic version use instead `--level required`\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8919768",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.2.1 Further settings for finalising the configuration file\n",
    "\n",
    "The  important keys, specific to helical case, are discussed at [this link](https://tomotools.gitlab-pages.esrf.fr/nabu/helical.html).\n",
    "To finalise the configuration file you need to \n",
    "\n",
    "- set the Paganin options\n",
    "- set the `processes_file` parameter for weights map and double flat:\n",
    "- set rotation_axis_position\n",
    "- specify the output file/directory in the [output] section\n",
    "- select the reconstruction range either:\n",
    "    - by:\n",
    "        - start_z, end_z\n",
    "    - or:\n",
    "        - start_z_nn, end_z_mm\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26a2f183",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.2.2 - Rotation axis position\n",
    "\n",
    "A command line utility `nabu-composite-cor` provides a roboust algorithm for cor finding. The actual version applies to standard tomography scan, and can be used also for helical scans when they have a first part of the acquisition which is done at fixed `z` translation. The actual bliss helical scan have this property. If there is enough information in this first part the algorithm will work.\n",
    "The full documentation can be found at [this link](https://tomotools.gitlab-pages.esrf.fr/nabu/cli_tools.html#nabu-composite-cor-application-to-extract-the-center-of-rotation-for-a-scan-or-a-series-of-scans).\n",
    "It has a full set of features which makes it suited for dealing also with z-series, but for our present case it usage can be as simple as \n",
    "\n",
    " \n",
    "````bash\n",
    "nabu-composite-cor --filename_template ${sn}.nx \\\n",
    "--cor_options \"side='near'; near_pos = 700.0; near_width = 20.0\"\n",
    "\n",
    "````\n",
    "which writes the cor, or the list of cors for z-series, in the default output file:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24be9f37",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Processes file\n",
    "\n",
    "We create the file containing the weights map and default double flat field. the weights will be based on the flats of the dataset. The double flat-filed will be set to one. \n",
    "We do this with the following instruction\n",
    " \n",
    "````bash\n",
    "    transition_width=60\n",
    "    nabu-helical-prepare-weights-double \\\n",
    "    $sn.nx \\\n",
    "    entry0000 \\\n",
    "    weights_and_double.h5 \\\n",
    "    $transition_width\n",
    "\n",
    "````\n",
    "The fourth and fifth arguments are optional. The transition_width, which defaults to `50` pixel, determines how the weights are apodised near the upper and lower border. \n",
    "The name of the produced file must be provided in the `preproc` section:\n",
    "\n",
    "````\n",
    "   [preproc]\n",
    "   ...\n",
    "   processes_file =  weights_and_double.h5\n",
    "\n",
    "````"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4e681e9",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## 1.3 - Helical reconstruction\n",
    "\n",
    "Let's run our first reconstruction.\n",
    "\n",
    "to select a vertical reconstruction range it may be very useful to get informations about the doable vertical span. This is done with the following command:\n",
    "\n",
    "````bash\n",
    "nabu-helical apple.conf --dry_run 1\n",
    "````\n",
    "````\n",
    "\n",
    "        Doable vertical span\n",
    "        --------------------\n",
    "             The scan has been performed with an descending vertical \n",
    "             translation of the rotation axis.\n",
    "              \n",
    "             The detector vertical axis is up side down.\n",
    "\n",
    "             Detector reference system at iproj=0:\n",
    "                from vertical view height  ...   -2830\n",
    "                up to  (included)     ...   543\n",
    "\n",
    "                 The slice that projects to the first line of the first projection \n",
    "                 corresponds to vertical heigth = 0\n",
    "\n",
    "             In voxels, the vertical doable span measures: 3373.0\n",
    "\n",
    "             And in millimiters above the stage:\n",
    "                 from vertical height above stage ( mm units)  ...   49.66862\n",
    "                 up to  (included)                                ...  128.32698\n",
    "\n",
    "\n",
    "\n",
    "````\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8843e10",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "#### Exercise\n",
    "\n",
    "Read the documentation [at this link](https://tomotools.gitlab-pages.esrf.fr/nabu/helical.html). about `start_z_mm` and `end_z_mm`. Then, knowing that the scan starts approximatively at the bottom of the apple, guess the `start_z_mm` and `end_z_mm` to reconstruct the seeds.(Hint: [market preferences](https://en.wikipedia.org/wiki/Apple) for apples).\n",
    "\n",
    "Finally to reconstruct\n",
    "\n",
    "The syntax is `nabu-helical apple.conf` \n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91593d9d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
