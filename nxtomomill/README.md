recommended order:

* nxtomomill_introduction.ipynb
* h52nx_conversion.ipynb
* (edf2nx_conversion.ipynb) -> only of you intend to convert from EDF to NXtomo
* (dxfile2nx_conversion.ipynb) -> only of you intend to convert from dxfile to NXtomo
* nxtomomill_edition.ipynb
* what_is_next.ipynb
