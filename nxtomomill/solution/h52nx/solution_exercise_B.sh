#!/bin/bash

echo "start conversion of a bliss file to NXtomo from a configuration file"
echo "current directory is $(pwd)"
nxtomomill h5-config my_configuration_file.cfg
nxtomomill h52nx bambou_hercules_0001.h5 bambou_hercules_0001.nx --config my_configuration_file.cfg
silx view bambou_hercules_0001.nx
