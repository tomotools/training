#!/bin/bash

echo "start conversion of a bliss file to NXtomo from a configuration redefining the sequence to be converted"
echo "current directory is $(pwd)"
nxtomomill h52nx --config conversion_using_data_scans.cfg
silx view bambou_from_data_scans.nx
