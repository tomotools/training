#!/bin/python

from nxtomomill.converter import from_h5_to_nx
from nxtomomill.io.config import TomoHDF5Config

input_file_path = "bambou_hercules_0001_different_titles.h5"

configuration = TomoHDF5Config()
configuration.input_file = input_file_path
configuration.output_file = "bambou_hercules_0001_different_titles.nx"
configuration.init_titles = ("mytomo:fullturn",)
configuration.projections_titles = ("my_projections",)

res = from_h5_to_nx(configuration=configuration)
