#!/bin/python

from nxtomomill.converter import from_h5_to_nx
from nxtomomill.io.config import TomoHDF5Config

input_file_path = "bambou_hercules_0001.h5"

configuration = TomoHDF5Config()
configuration.input_file = input_file_path
configuration.output_file = "bambou_hercules_0001.nx"

res = from_h5_to_nx(configuration=configuration)
