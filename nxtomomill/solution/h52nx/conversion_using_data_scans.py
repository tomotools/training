#!/bin/python

from nxtomomill.converter import from_h5_to_nx
from nxtomomill.io.config import TomoHDF5Config
from nxtomomill.io.framegroup import FrameGroup
from silx.io.url import DataUrl

input_file_path = "bambou_hercules_0001.h5"

configuration = TomoHDF5Config()
configuration.input_file = input_file_path
configuration.output_file = "bambou_hercules_0001.nx"
configuration.data_frame_grps = (
    FrameGroup(
        url=DataUrl(
            file_path=input_file_path,
            data_path="1.1",
            scheme="silx",
        ),
        frame_type="initialization",
    ),
    FrameGroup(
        url=DataUrl(
            file_path=input_file_path,
            data_path="2.1",
            scheme="silx",
        ),
        frame_type="darks",
    ),
    FrameGroup(
        url=DataUrl(
            file_path=input_file_path,
            data_path="3.1",
            scheme="silx",
        ),
        frame_type="flats",
    ),
    FrameGroup(
        url=DataUrl(
            file_path=input_file_path,
            data_path="4.1",
            scheme="silx",
        ),
        frame_type="projections",
    ),
)

res = from_h5_to_nx(configuration=configuration)
