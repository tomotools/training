#!/bin/bash

echo "start conversion of a bliss file to NXtomo redefining some path / keys to be used (for rotation angle in this case)"
echo "current directory is $(pwd)"
nxtomomill h52nx bambou_hercules_0001_exotic_rot_angle.h5 bambou_hercules_0001_exotic_rot_angle.nx --rot-angle-keys myrot
silx view bambou_hercules_0001_exotic_rot_angle.nx
