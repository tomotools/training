#!/bin/bash

echo "start conversion of a bliss file to NXtomo redefining some title names"
echo "current directory is $(pwd)"
nxtomomill h52nx bambou_hercules_0001_different_titles.h5 bambou_hercules_0001_different_titles.nx --init_titles="mytomo:fullturn" --proj_titles="my_projections"
silx view bambou_hercules_0001_different_titles.nx
