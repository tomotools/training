#!/bin/bash

cp bambou_hercules_0001.nx bambou_hercules_0001_patch_2.nx
nxtomomill patch-nx bambou_hercules_0001_patch_2.nx entry0000 --darks-at-start "silx:mydarks.h5?path=data&slice=2:6"
silx view bambou_hercules_0001_patch_2.nx
