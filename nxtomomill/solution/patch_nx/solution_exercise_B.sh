#!/bin/bash

cp bambou_hercules_0001.nx bambou_hercules_0001_patch_1.nx
nxtomomill patch-nx bambou_hercules_0001_patch_1.nx entry0000 --darks-at-start "silx:mydarks.h5?path=data"
silx view bambou_hercules_0001_patch_1.nx
