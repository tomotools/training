#!/bin/bash

echo "copy a NXtomo and invalid the first two frames"
echo "current directory is $(pwd)"
cp bambou_hercules_0001.nx bambou_hercules_0001_frame_updated.nx
nxtomomill patch-nx bambou_hercules_0001_frame_updated.nx entry0000 --invalid-frames 0:2
silx view bambou_hercules_0001_frame_updated.nx
