import numpy
from nxtomo.application.nxtomo import NXtomo

nxtomo = NXtomo().load(
    "crayon_broken.nx",
    "entry0000",
)

nxtomo.sample.rotation_angle = numpy.rad2deg(nxtomo.sample.rotation_angle)
nxtomo.instrument.detector.field_of_view = "Half"
nxtomo.instrument.detector.distance = 0.012
nxtomo.instrument.detector.distance.units = "m"
nxtomo.save(
    "crayon_fixed.nx",
    "entry0000",
    overwrite=True,
)    
